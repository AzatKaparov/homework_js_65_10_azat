import {BrowserRouter, Route, Switch} from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Layout from "./Components/UI/Layout/Layout";
import MainContainer from "./Components/MainContainer";
import ShowPage from "./Containers/Pages/ShowPage";
import EditPage from "./Containers/Pages/EditPage";

function App() {
  return (
    <div className="app">
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route path="/" exact component={MainContainer}/>
                    <Route path="/pages/admin" exact component={EditPage}/>
                    <Route path="/pages/:pageName" exact component={ShowPage}/>
                    <Route render={() => <h1>Not found</h1>}/>
                </Switch>
            </Layout>
        </BrowserRouter>
    </div>
  );
}

export default App;
