export const PAGES_LIST = [
    {title: "About", id: "about"},
    {title: "Contacts", id: "contacts"},
    {title: "Orders", id: "orders"},
    {title: "Complaints", id: "complaints"},
    {title: "Suggestions", id: "suggestions"},
];