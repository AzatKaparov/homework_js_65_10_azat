import React from 'react';
import {NavLink} from "react-router-dom";

const NavigationItem = ({to, exact, children}) => {
    return (
        <li className="NavigationItem">
            <NavLink className="mx-2" to={to} exact={exact}>{children}</NavLink>
        </li>
    );
};

export default NavigationItem;