import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";
import {Nav, Navbar} from "react-bootstrap";
import {PAGES_LIST} from "../../../constants";

const NavigationItems = () => {
    return (
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
                <NavigationItem to="/" exact>Home</NavigationItem>
                <NavigationItem to="/pages/admin" exact>Admin</NavigationItem>
                {PAGES_LIST.map(item => {
                    return (
                        <NavigationItem key={item.id} to={`/pages/${item.id}`}>{item.title}</NavigationItem>
                    )
                })}
            </Nav>
        </Navbar.Collapse>
    );
};

export default NavigationItems;