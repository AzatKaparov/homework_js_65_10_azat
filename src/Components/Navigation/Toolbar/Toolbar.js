import React from 'react';
import NavigationItems from "../NavigationItems/NavigationItems";
import {Container, Navbar} from "react-bootstrap";

const Toolbar = () => {
    return (
        <header className="Toolbar">
            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand href="/">PageControl</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <NavigationItems/>
                </Container>
            </Navbar>
        </header>
    );
};

export default Toolbar;