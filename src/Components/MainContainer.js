import React from 'react';

const MainContainer = () => {
    return (
        <div className="container">
            <h1 className="text-center my-5">This is main page</h1>
        </div>
    );
};

export default MainContainer;