import React, {useEffect, useState} from 'react';
import {Button, Form} from 'react-bootstrap';
import {PAGES_LIST} from "../../constants";
import axiosApi from "../../axiosApi";

const EditPage = ({ history }) => {
    const [pageName, setPageName] = useState('');

    const [selectedOptions, setSelectedOptions] = useState({
        title: '',
        content: '',
    });

    const pageNameChanged = e => {
        setPageName(e.target.value);
    };

    const dataChanged = e => {
        const name = e.target.name;
        const value = e.target.value;

        setSelectedOptions(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const pageSendHandler = async e => {
        e.preventDefault();
        const page = {
            title: selectedOptions.title,
            content: selectedOptions.content,
        };
        try {
            await axiosApi.put(`./pages/${pageName}.json`, page);
        } finally {
            history.push(`./${pageName}`);
        }
    };


    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get(`./pages/${pageName}.json`);
            const data = response.data;
            setSelectedOptions({
                title: data.title,
                content: data.content,
            });
        };
        if (pageName !== '') {
            fetchData().catch(console.error);
        }
    }, [pageName]);

    return (
        <div className="container my-4">
            <h1>Edit pages</h1>
            <Form onSubmit={pageSendHandler}>
                <Form.Group>
                    <Form.Control name="pageName" onChange={pageNameChanged} as="select" aria-label="Select category">
                        <option value="">Choose</option>
                        {PAGES_LIST.map(page => (
                            <option key={page.id}  value={page.id}>{page.title}</option>
                        ))}
                    </Form.Control>
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Title</Form.Label>
                    <Form.Control
                        type="text"
                        name="title"
                        value={selectedOptions.title}
                        onChange={dataChanged}
                    />
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Content</Form.Label>
                    <Form.Control
                        as="textarea"
                        name="content"
                        rows={3}
                        value={selectedOptions.content}
                        onChange={dataChanged}
                    />
                </Form.Group>
                <Button type="submit">SAVE</Button>
            </Form>
        </div>
    );
};

export default EditPage;