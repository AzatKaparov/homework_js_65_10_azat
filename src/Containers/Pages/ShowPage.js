import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";

const ShowPage = ({match}) => {
    const [page, setPage] = useState({});
    const pageName = match.params.pageName;

    useEffect(()=> {
        const fetchData = async () => {
            const response = await axiosApi.get(`./pages/${match.params.pageName}.json`);
            setPage(response.data);
        }
        fetchData().catch(console.error);
    }, [pageName]);

    return page && (
        <div className="container my-3">
            <h1>{page.title}</h1>
            <p>{page.content}</p>
        </div>
    );
};

export default ShowPage;