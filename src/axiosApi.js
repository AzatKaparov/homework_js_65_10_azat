import axios from 'axios';

const axiosApi = axios.create({
    baseURL: 'https://homework-65-1b3c6-default-rtdb.firebaseio.com/'
});

export default axiosApi;